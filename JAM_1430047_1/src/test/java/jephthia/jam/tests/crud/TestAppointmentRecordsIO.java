package jephthia.jam.tests.crud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;

//TODO use a random string generator to test the length of the field
//so that if we change the database's specs we can easily change the length size
/**
 * @author Jephthia Louis
 */
@Ignore
public class TestAppointmentRecordsIO
{
    // This is my local MySQL server
	private final String url = "jdbc:mysql://localhost:3306/JAMDB?autoReconnect=true&useSSL=false&noAccessToProcedureBodies=true"; 
    private final String user = "jephthia";
    private final String password = "database";
    
    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    
    @Test(timeout=1000)
    public void testGetID() throws SQLException
    {
    	AppointmentRecord expectedRecord = new AppointmentRecord(1, "neque", "144 South Pass", Timestamp.valueOf("2017-02-11 14:04:06"), Timestamp.valueOf("2017-08-08 04:50:33"), "sagittis nam congue risus semper porta volutpat quam pede lobortis ligula", true, 1, false);
    	 	
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	AppointmentRecord resultRecord = recordIO.getByID(1);
    	
    	assertEquals(expectedRecord, resultRecord);
    }
    
    @Test(timeout=1000)
    public void testUpdateRow() throws SQLException
    {
    	AppointmentRecord expectedRecord = new AppointmentRecord(1, "updatetitle", "updatelocation", Timestamp.valueOf("2017-03-11 14:04:06"), Timestamp.valueOf("2017-08-09 04:50:33"), "updatedetails", false, 2, true);
    	
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	AppointmentRecord recordToUpdate = new AppointmentRecord(1, "updatetitle", "updatelocation", Timestamp.valueOf("2017-03-11 14:04:06"),
    															Timestamp.valueOf("2017-08-09 04:50:33"), "updatedetails", false, 2, true);
    	
    	recordIO.updateRow(recordToUpdate);
    	
    	AppointmentRecord resultRecord = recordIO.getByID(1);
    	
    	assertEquals(expectedRecord, resultRecord);
    }
    
    @Test(timeout=1000, expected=SQLException.class)
    public void testFailUpdateRow() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	AppointmentRecord recordToUpdate = new AppointmentRecord(1, "updatetiJHHUUHIHUIUHIIHUIOIIDTYYYTthjgjhgtle",
    											"updatelocationhjgjghjgjhgjhjghgjgjjghjgjjhgjhjgjhjkhkjhkjhjk",
    											Timestamp.valueOf("2017-03-11 14:04:06"), Timestamp.valueOf("2017-08-09 04:50:33"),
    											"updatedetails", false, 2, true);
    	
    	recordIO.updateRow(recordToUpdate);
    	
    	fail("We expected the update to throw an exception because the string's"
    			+ "length is too big but it did not.");
    }
    
    @Test(timeout=1000)
    public void testDeleteRow() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	recordIO.deleteRow(1);
    	
    	AppointmentRecord resultRecord = recordIO.getByID(1);
    	
    	assertNull(resultRecord);
    }
    
    @Test(timeout=1000)
    public void testAddRow() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	AppointmentRecord recordToAdd = new AppointmentRecord(-1, "newtitle", "newlocation", Timestamp.valueOf("2017-03-11 14:04:06"),
														Timestamp.valueOf("2017-08-09 04:50:33"), "newdetails", false, 2, true);
    	
    	int insertedID = recordIO.addRow(recordToAdd);
    	
    	AppointmentRecord expectedRecord = new AppointmentRecord(insertedID, "newtitle", "newlocation", Timestamp.valueOf("2017-03-11 14:04:06"),
    																	Timestamp.valueOf("2017-08-09 04:50:33"), "newdetails", false, 2, true);
    	
    	AppointmentRecord resultRecord = recordIO.getByID(insertedID);
    	
    	assertEquals(expectedRecord, resultRecord);
    }
    
    @Test(timeout=1000, expected=SQLException.class)
    public void testFailAddRow() throws SQLException
    {
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	AppointmentRecord recordToAdd = new AppointmentRecord(-1, "newtihjkhhkjhkjhjjhhkjkjhjhkkjhkjhkjhhkjkjhjkhtle",
    										"newlocathuiuihuhjkjhkjhkjhjkhhkjhkjhkjhkjhkjhkjion", Timestamp.valueOf("2017-03-11 14:04:06"),
    										Timestamp.valueOf("2017-08-09 04:50:33"), "newdetails", false, 2, true);
    	
    	recordIO.addRow(recordToAdd);
    	
    	fail("We expected the add to throw an exception because the string's"
    			+ "length is too big but it did not.");
    }
    
    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, the lead Arquillian developer
     * at JBoss
     */
    @Before public void seedDatabase()
    {
        log.info("Seeding Database");
        
        final String seedDataScript = loadAsString("CreateAppointmentRecordsTable.sql");
        
        try (Connection connection = DriverManager.getConnection(url, user, password))
        {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";"))
            {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path)
    {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
             Scanner scanner = new Scanner(inputStream);)
        {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter)
    {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        
        try
        {
            String line;
            
            while ((line = bufferedReader.readLine()) != null)
            {
                line = line.trim();
                if (line.isEmpty() || isComment(line))
                {
                    continue;
                }
                
                sqlStatement.append(line);
                
                if (line.endsWith(statementDelimiter))
                {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line)
    {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
