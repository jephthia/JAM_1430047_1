drop database if exists jamdb;
create database jamdb;
use jamdb;

drop user if exists jephthia@localhost;
create user jephthia@'localhost' identified by 'database';
grant all on jamdb.* to jephthia@'localhost';
flush privileges;
