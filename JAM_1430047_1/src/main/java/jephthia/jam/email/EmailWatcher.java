package jephthia.jam.email;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;

public class EmailWatcher implements Runnable
{
	private final Logger log = LoggerFactory.getLogger(getClass().getName());
	
	private EmailSender emailSender;
	private List<AppointmentRecord> todaysAppointments = new ArrayList<AppointmentRecord>();
	private int reminderInterval;
	
	public EmailWatcher() throws SQLException
	{
		emailSender = new EmailSender();
	
		reminderInterval = emailSender.getDefaultSmtp().getReminderInterval();
		
		//Get all the appointments that are set for today
		//and only keep the ones that have their reminder
		//set to true.
		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
		for(AppointmentRecord record : recordIO.getByDate(Timestamp.valueOf(LocalDate.now().atStartOfDay())))
		{
			if(record.isAlarmReminder())
				todaysAppointments.add(record);
		}
	}
	
	@Override public void run()
	{
		// Platform.runLater() allows this thread to interact with the main
        // JavaFX thread and therefore interact with controls.
        Platform.runLater(() -> {
        	
        	LocalDateTime startInterval = LocalDateTime.now().plusMinutes(reminderInterval).minusSeconds(29);
        	LocalDateTime endInterval = LocalDateTime.now().plusMinutes(reminderInterval).plusSeconds(29);
        	
        	for(AppointmentRecord record : todaysAppointments)
        	{
        		//Check if appointment is 30 seconds before the reminder interval or
        		//30 seconds after the reminder interval
        		if(record.getStartTime().toLocalDateTime().isAfter(startInterval) &&
        				record.getStartTime().toLocalDateTime().isBefore(endInterval))
        		{
        			emailSender.sendEmail(record);
        			
        			log.info("Sent an email");
        		}
        	}

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sending An Email");
            alert.setHeaderText("Sending An Email.");
            alert.setContentText("An email has been sent.");
            alert.showAndWait();
        });
	}
}
