package jephthia.jam.entities;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Jephthia Louis
 */
public class Smtp
{
	private IntegerProperty id;
    private StringProperty name;
    private StringProperty email;
    private StringProperty password;
    private StringProperty serverUrl;
    private IntegerProperty port;
    private IntegerProperty reminderInterval;
    private BooleanProperty isDefault;

    public Smtp()
    {
    	this(0, "", "", "", "", 0, 0, false);
    }
    
    public Smtp(int id, String name, String email, String password, String serverUrl, int port, int reminderInterval, boolean isDefault)
    {
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.email = new SimpleStringProperty(email);
		this.password = new SimpleStringProperty(password);
		this.serverUrl = new SimpleStringProperty(serverUrl);
		this.port = new SimpleIntegerProperty(port);
		this.reminderInterval = new SimpleIntegerProperty(reminderInterval);
		this.isDefault = new SimpleBooleanProperty(isDefault);
	}

	public int getId()
    {
        return id.get();
    }
	
	public IntegerProperty getIdProperty()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getName()
    {
        return name.get();
    }
    
    public StringProperty getNameProperty()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = new SimpleStringProperty(name);
    }

    public String getEmail()
    {
        return email.get();
    }
    
    public StringProperty getEmailProperty()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = new SimpleStringProperty(email);
    }

    public String getPassword()
    {
        return password.get();
    }
    
    public StringProperty getPasswordProperty()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = new SimpleStringProperty(password);
    }

    public String getServerUrl()
    {
        return serverUrl.get();
    }
    
    public StringProperty getServerUrlProperty()
    {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl)
    {
        this.serverUrl = new SimpleStringProperty(serverUrl);
    }

    public int getPort()
    {
        return port.get();
    }
    
    public IntegerProperty getPortProperty()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = new SimpleIntegerProperty(port);
    }

    public boolean isDefault()
    {
        return isDefault.get();
    }
    
    public BooleanProperty isDefaultProperty()
    {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault)
    {
        this.isDefault = new SimpleBooleanProperty(isDefault);
    }
    
    public int getReminderInterval()
    {
		return reminderInterval.get();
	}
    
    public IntegerProperty getReminderIntervalProperty()
    {
		return reminderInterval;
	}

	public void setReminderInterval(int reminderInterval)
	{
		this.reminderInterval = new SimpleIntegerProperty(reminderInterval);
	}
    
    @Override public int hashCode()
    {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
		result = prime * result + getId();
		result = prime * result + (isDefault() ? 1231 : 1237);
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
		result = prime * result + getPort();
		result = prime * result + getReminderInterval();
		result = prime * result + ((getServerUrl() == null) ? 0 : getServerUrl().hashCode());
		return result;
	}

	@Override public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Smtp other = (Smtp)obj;
		
		if (getEmail() == null)
		{
			if (other.getEmail() != null)
				return false;
		}
		else if (!getEmail().equals(other.getEmail()))
			return false;
		
		if (getId() != other.getId())
			return false;
		
		if (isDefault() != other.isDefault())
			return false;
		
		if (getName() == null)
		{
			if (other.getName() != null)
				return false;
		}
		else if (!getName().equals(other.getName()))
			return false;
		
		if (getPassword() == null)
		{
			if (other.getPassword() != null)
				return false;
		}
		else if (!getPassword().equals(other.getPassword()))
			return false;
		
		if (getPort() != other.getPort())
			return false;
		
		if (getReminderInterval() != other.getReminderInterval())
			return false;
		
		if (getServerUrl() == null)
		{
			if (other.getServerUrl() != null)
				return false;
		}
		else if (!getServerUrl().equals(other.getServerUrl()))
			return false;
		
		return true;
	}

	@Override public String toString()
	{
		return "Smtp [id=" + getId() + ", name=" + getName() + ", email=" + getEmail() + ", password=" + getPassword() + ", server_url="
				+ getServerUrl() + ", port=" + getPort() + ", reminderInterval=" + getReminderInterval() + ", isDefault=" + isDefault() + "]";
	}
}
