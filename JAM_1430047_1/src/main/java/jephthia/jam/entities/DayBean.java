package jephthia.jam.entities;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.VBox;

public class DayBean
{
	private StringProperty hour;
	private SimpleObjectProperty<VBox> appointmentsContainer;
	
	public DayBean()
	{
		this("", new VBox());
	}
	
	public DayBean(String hour, VBox appointmentsContainer)
	{
		this.hour = new SimpleStringProperty(hour);
		this.appointmentsContainer = new SimpleObjectProperty<VBox>(appointmentsContainer);
	}
	
	public String getHour()
	{
		return hour.get();
	}
	
	public StringProperty getHourProperty()
	{
		return hour;
	}

	public void setHour(String hour)
	{
		this.hour.set(hour);
	}
	
	public SimpleObjectProperty<VBox> getAppointmentsContainerProperty()
	{
		return appointmentsContainer;
	}
	
	public VBox getAppointmentsContainer()
	{
		return appointmentsContainer.get();
	}

	public void setAppointmentsContainer(VBox appointmentsContainer)
	{
		this.appointmentsContainer.set(appointmentsContainer);
	}
}
