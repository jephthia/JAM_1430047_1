package jephthia.jam.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.ConnectionJDBC;
import jephthia.jam.entities.AppointmentGroup;

/**
 * @author Jephthia Louis
 */
public class AppointmentGroupsIO
{
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());
    
    /**
     * Create a new Appointment Group, the appointment
     * group passed will be updated with the id that was set
     * @param group
     * @return
     * @throws SQLException
     */
    public int addRow(AppointmentGroup group) throws SQLException
    {
    	int insertedID = -1;
    	
    	log.debug("appointmentgroups addRow called");
    	
    	String query = "{call appointment_groups_add(?,?)}";
    	
    	try(Connection con = new ConnectionJDBC().getConnection();
    		CallableStatement callSmtp = con.prepareCall(query))
    	{	
			callSmtp.setString(1, group.getGroupName());
			callSmtp.setString(2, group.getColor());

			callSmtp.executeUpdate();
			
			log.debug("appointment group added");
			
			String lastID = "select last_insert_id()";
			
			PreparedStatement stmt = con.prepareStatement(lastID);
			ResultSet resultSet = stmt.executeQuery();
			
			if(resultSet.next())
				insertedID = resultSet.getInt("last_insert_id()");
			
			group.setGroupNumber(insertedID);
			
    	} catch (SQLException e) {
			throw e;
		}
    	
    	return insertedID;
    }
    
    /**
     * Finds the appointment group with the same
     * group number as the one passed and the updates
     * it with the new values of the group passed.
     * @param group
     * @throws SQLException
     */
    public void updateRow(AppointmentGroup group) throws SQLException
    {
    	log.debug("appointmentgroups updateRow called");
    	
    	String query = "{call appointment_groups_update(?,?,?)}";
    	
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{		
			callSmtp.setInt(1, group.getGroupNumber());
			callSmtp.setString(2, group.getGroupName());
			callSmtp.setString(3, group.getColor());

			callSmtp.executeUpdate();
			
			log.debug("appointment group updated");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    /**
     * Deletes the appointment group that
     * matches this group number, if the group number
     * doesn't exists it will do nothing.
     * @param groupNumber
     * @throws SQLException
     */
    public void deleteRow(int groupNumber) throws SQLException
    {
    	log.debug("appointmentgroups deleteRow called");
    	
    	String query = "{call appointment_groups_delete(?)}";
    	
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{		
			callSmtp.setInt(1, groupNumber);
			callSmtp.executeUpdate();
			
			log.debug("appointment group deleted");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    /**
     * Returns the appointment group with the
     * matching group number, if no group was
     * found null is returned.
     * @param groupNumber
     * @return
     * @throws SQLException
     */
    public AppointmentGroup getRow(int groupNumber) throws SQLException
    {
        log.debug("appointmentGroup getRow called");
    
        String query = "select * from appointment_groups where group_number = ?";
	
        try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{		
			stmt.setInt(1, groupNumber);
			
			ResultSet results = stmt.executeQuery();
    
			if(results.next())
			{
				AppointmentGroup group = new AppointmentGroup();
				group.setGroupNumber(results.getInt("group_number"));
				group.setGroupName(results.getString("group_name"));
				group.setColor(results.getString("color"));
			    
			    return group;
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return null;
    }
    
    /**
     * Returns all the current appointment groups
     * in the database, if no groups found, an
     * empty list is returned.
     * @return
     * @throws SQLException
     */
    public ArrayList<AppointmentGroup> getAll() throws SQLException
    {
        log.debug("appointmentGroup getRow called");
    
        String query = "select * from appointment_groups";
	
        ArrayList<AppointmentGroup> groups = new ArrayList<AppointmentGroup>();
        
        try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{	
			ResultSet results = stmt.executeQuery();
    
			while(results.next())
			{
				AppointmentGroup group = new AppointmentGroup();
				group.setGroupNumber(results.getInt("group_number"));
				group.setGroupName(results.getString("group_name"));
				group.setColor(results.getString("color"));
			    
			    groups.add(group);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return groups;
    }
}
