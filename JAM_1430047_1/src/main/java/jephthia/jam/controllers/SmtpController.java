package jephthia.jam.controllers;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import jephthia.jam.crud.SmtpIO;
import jephthia.jam.entities.Smtp;

public class SmtpController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private Smtp smtp;
	
    @FXML private TextField nameTextField;

    @FXML private TextField emailTextField;

    @FXML private TextField passwordTextField;

    @FXML private TextField serverUrlTextField;

    @FXML private TextField portTextField;

    @FXML private TextField reminderIntervalTextField;

    @FXML private CheckBox isDefaultCheckBox;
    
    public void setSmtp(Smtp smtp)
    {
    	this.smtp = smtp;
    	doBindings();
    }
    
    /*
     * Binds the fields to the smtp's values
     */
    private void doBindings()
    {
    	Bindings.bindBidirectional(nameTextField.textProperty(), smtp.getNameProperty());
    	Bindings.bindBidirectional(emailTextField.textProperty(), smtp.getEmailProperty());
    	Bindings.bindBidirectional(passwordTextField.textProperty(), smtp.getPasswordProperty());
    	Bindings.bindBidirectional(serverUrlTextField.textProperty(), smtp.getServerUrlProperty());
    	Bindings.bindBidirectional(portTextField.textProperty(), smtp.getPortProperty(), new NumberStringConverter());
    	Bindings.bindBidirectional(reminderIntervalTextField.textProperty(), smtp.getReminderIntervalProperty(), new NumberStringConverter());
    	Bindings.bindBidirectional(isDefaultCheckBox.selectedProperty(), smtp.isDefaultProperty());
    }
    
    @FXML private void handleCreateSmtp(ActionEvent event)
    {
    	log.debug("SMTP: " + smtp.toString());
    	
    	try
    	{
    		SmtpIO smtpIO = new SmtpIO();
			smtpIO.addRow(smtp);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
    
    @FXML private void handleEditSmtp(ActionEvent event)
    {
    	log.debug("SMTP: " + smtp.toString());
    	
    	try
    	{
    		SmtpIO smtpIO = new SmtpIO();
			smtpIO.updateRow(smtp);
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
    
    @FXML private void handleDeleteSmtp(ActionEvent event)
    {
    	log.debug("SMTP: " + smtp.toString());
    	
    	try
    	{
    		SmtpIO smtpIO = new SmtpIO();
			smtpIO.deleteRow(smtp.getId());
			
			final Node source = (Node) event.getSource();
		    final Stage stage = (Stage) source.getScene().getWindow();
		    stage.close();
		}
    	catch (SQLException e)
    	{
			e.printStackTrace();
		}
    }
}
