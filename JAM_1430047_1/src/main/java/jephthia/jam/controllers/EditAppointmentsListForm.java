package jephthia.jam.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;

public class EditAppointmentsListForm
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private List<AppointmentRecord> appointments;
	
    @FXML private ChoiceBox<String> appointmentsChoiceBox;

    /**
     * Sets the list of appointments so that we can display
     * a choice box to the user
     * @param appointments
     */
    public void setAppointments(List<AppointmentRecord> appointments)
    {
    	this.appointments = appointments;
    	
    	for(AppointmentRecord record : appointments)
    		appointmentsChoiceBox.getItems().add(record.getTitle() + " Start Time: " + record.getStartTime() + " End Time: " + record.getEndTime());
    }
    
    @FXML private void handleEdit(ActionEvent event)
    {
    	//Get the appointment that was selected and
    	//open the form to edit that appointment
    	AppointmentRecord appointment = appointments.get(appointmentsChoiceBox.getSelectionModel().getSelectedIndex());
    	
    	ArrayList<AppointmentGroup> groups = new ArrayList<AppointmentGroup>();
		
		try
		{
			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			groups = groupIO.getAll();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}		
		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainController.class.getResource("/fxml/AppointmentForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			Stage stage = new Stage();
			stage.setTitle("New Appointment Record");
			
			// Put the Scene on Stage
			stage.setScene(scene);
			stage.show();
			
			AppointmentController controller = loader.getController();
			
			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
			AppointmentGroup appointmentGroup = groupIO.getRow(appointment.getAppointmentGroup());
			
			controller.setAppointmentRecord(appointment, appointmentGroup, groups);
			
			//((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException | SQLException ex) {
		    log.error(null, ex);
		}
    }
}