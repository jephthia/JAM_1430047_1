#--------------#
#   DATABASE   #
#--------------#
drop database if exists jamdb;
create database jamdb;
use jamdb;

drop user if exists jephthia@localhost;
create user jephthia@'localhost' identified by 'database';
grant all on jamdb.* to jephthia@'localhost';
flush privileges;


#--------------#
#    DROPS     #
#--------------#
drop table if exists smtp;
drop table if exists appointment_records;
drop table if exists appointment_groups;

#--------------#
#    TABLES    #
#--------------#
create table smtp
(
	id int not null auto_increment primary key,
	name VARCHAR(30),
    email VARCHAR(30),
    email_password VARCHAR(30),
    server_url VARCHAR(30),
    port int(6) default 465,
    reminder_interval int,
    isDefault boolean default false
);

create table appointment_groups
(
	group_number int not null auto_increment primary key,
    group_name varchar(20),
    color varchar(10)
);

create table appointment_records
(
	id int not null auto_increment primary key,
	title varchar(30),
	location varchar(30),
	start_time timestamp,
	end_time timestamp,
	details text,
	whole_day boolean,
	appointment_group int,
	alarm_reminder boolean
);


delimiter $


#--------------#
#  SMTP CRUD   #
#--------------#

#--------------#
#     ADD      #
#--------------#
drop procedure if exists smtp_add$
create procedure smtp_add(in _name varchar(30), in _email varchar(30), in _email_password varchar(30), in _server_url varchar(30), in _port int(6), in _reminder_interval int, in _isDefault boolean)
begin
	insert into smtp values(null, _name, _email, _email_password, _server_url, _port, _reminder_interval, _isDefault);
end$

#--------------#
#    UPDATE    #
#--------------#
drop procedure if exists smtp_update$
create procedure smtp_update(in _id int, in _name varchar(30), in _email varchar(30), in _email_password varchar(30), in _server_url varchar(30), in _port int(6), in _reminder_interval int, in _isDefault boolean)
begin
	update smtp set name = _name, email = _email, email_password = _email_password, server_url = _server_url, port = _port , reminder_interval = _reminder_interval where id = _id;
end$

#--------------#
#    DELETE    #
#--------------#
drop procedure if exists smtp_delete$
create procedure smtp_delete(in _id int)
begin
	delete from smtp where id = _id;
end$

#--------------------------#
#  APPOINTMENT GROUP CRUD  #
#--------------------------#

#--------------#
#     ADD      #
#--------------#
drop procedure if exists appointment_group_add$
create procedure appointment_groups_add(in _group_name varchar(20), in _color varchar(10))
begin
	insert into appointment_groups values(null, _group_name, _color);
end$

#--------------#
#    UPDATE    #
#--------------#
drop procedure if exists appointment_group_update$
create procedure appointment_groups_update(in _group_number int(6), in _group_name varchar(20), in _color varchar(10))
begin
	update appointment_groups set group_name = _group_name, color = _color where group_number = _group_number;
end$

#--------------#
#    DELETE    #
#--------------#
drop procedure if exists appointment_group_delete$
create procedure appointment_groups_delete(in _group_number varchar(30))
begin
	delete from appointment_groups where group_number = _group_number;
end$


#--------------------------#
# APPOINTMENT RECORDS CRUD #
#--------------------------#

#--------------#
#     ADD      #
#--------------#
drop procedure if exists appointment_records_add$
create procedure appointment_records_add(in _title varchar(30), in _location varchar(30), in _start_time timestamp, in _end_time timestamp,
		in _details text, in _whole_day boolean, in _appointment_group int, in _alarm_reminder boolean)
begin
	insert into appointment_records values(null, _title, _location, _start_time, _end_time, _details, _whole_day, _appointment_group, _alarm_reminder);
end$

#--------------#
#    UPDATE    #
#--------------#
drop procedure if exists appointment_records_update$
create procedure appointment_records_update(in _id int, in _title varchar(30), in _location varchar(30), in _start_time timestamp, in _end_time timestamp,
		in _details text, in _whole_day boolean, in _appointment_group int, in _alarm_reminder boolean)
begin
	update appointment_records set title = _title, location = _location, start_time = _start_time, end_time = _end_time, details = _details, whole_day = _whole_day,
											appointment_group = _appointment_group, alarm_reminder = _alarm_reminder
											where id = _id;
end$

#--------------#
#    DELETE    #
#--------------#
drop procedure if exists appointment_records_delete$
create procedure appointment_records_delete(in _id int)
begin
	delete from appointment_records where id = _id;
end$

delimiter ;

