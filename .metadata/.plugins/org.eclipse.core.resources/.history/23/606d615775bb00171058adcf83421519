package jephthia.jam.controllers;

import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import jephthia.jam.entities.WeekDayBean;

public class WeeklyCalendarController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private int weekCounter = 0;
	
	@FXML private Label weekLabel;
	
    @FXML private TableView<WeekDayBean> weekTableView;

    @FXML private TableColumn<WeekDayBean, String> hourCol;
    
    @FXML private TableColumn<WeekDayBean, String> sundayCol;

    @FXML private TableColumn<WeekDayBean, String> mondayCol;

    @FXML private TableColumn<WeekDayBean, String> tuesdayCol;

    @FXML private TableColumn<WeekDayBean, String> wednesdayCol;

    @FXML private TableColumn<WeekDayBean, String> thursdayCol;

    @FXML private TableColumn<WeekDayBean, String> fridayCol;

    @FXML private TableColumn<WeekDayBean, String> saturdayCol;

    @FXML private void initialize() throws SQLException
    {
    	doBindings();
    	displayTable();
    }
    
    /**
     * Bind the columns to the bean so that
     * the table view can display the appropriate
     * appointments.
     */
    private void doBindings()
    {
    	hourCol.setCellValueFactory(celldata -> celldata.getValue().getHourProperty());
    	sundayCol.setCellValueFactory(celldata -> celldata.getValue().getSundayProperty());
    	mondayCol.setCellValueFactory(celldata -> celldata.getValue().getMondayProperty());
    	tuesdayCol.setCellValueFactory(celldata -> celldata.getValue().getTuesdayProperty());
    	wednesdayCol.setCellValueFactory(celldata -> celldata.getValue().getWednesdayProperty());
    	thursdayCol.setCellValueFactory(celldata -> celldata.getValue().getThursdayProperty());
    	fridayCol.setCellValueFactory(celldata -> celldata.getValue().getFridayProperty());
    	saturdayCol.setCellValueFactory(celldata -> celldata.getValue().getSaturdayProperty());
    }
    
    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTable() throws SQLException
    {
        // Add observable list data to the table
        weekTableView.setItems(findAllHours());
    }
    
    private ObservableList<WeekDayBean> findAllHours()
    {
    	weekLabel.setText("Week of " + LocalDate.now().plusWeeks(weekCounter - 1).with(DayOfWeek.SUNDAY).format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
    	
    	ObservableList<WeekDayBean> days = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < 24; i++)
    	{
    		WeekDayBean dayBean = new WeekDayBean();
    		dayBean.setHour(i + ":" + "00");
    		days.add(dayBean);
    		
    		WeekDayBean dayBeanHalf = new WeekDayBean();
    		dayBeanHalf.setHour(i + ":" + "30");
    		
    		days.add(dayBeanHalf);
    	}
    	
    	return days;
    }
    
    @FXML private void handleNextWeek(ActionEvent event) throws SQLException
    {
    	weekCounter++;
    	displayTable();
    }
    
    @FXML private void handlePreviousWeek(ActionEvent event) throws SQLException
    {
    	weekCounter--;
    	displayTable();
    }
}
