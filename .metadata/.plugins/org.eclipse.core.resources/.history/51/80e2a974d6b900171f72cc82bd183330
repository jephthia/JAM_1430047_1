package jephthia.jam;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jephthia.jam.email.EmailWatcher;

/**
 * @author Jephthia Louis
 */
public class MainAppFx extends Application
{
	private Logger log = LoggerFactory.getLogger(this.getClass().getName());
	
	// The primary window or frame of this application
    private Stage primaryStage;
    private final EmailWatcher emailWatcher;
    private final ScheduledExecutorService executor;
	
	@Override public void init()
	{
		emailWatcher = new EmailWatcher();
		// Multiple tasks can be scheduled, here we are scheduling just one
		executor = Executors.newScheduledThreadPool(1);
    }

	@Override public void start(Stage primaryStage)
	{
		log.info("Program Begins");
		
		this.primaryStage = primaryStage;
		
		// The interval is between the start time of the task.
        // The task must complete its task before the next interval completes.
        // Runnable task, delay to start, interval between tasks, time units
        executor.scheduleWithFixedDelay(emailTask, 20, 20, TimeUnit.SECONDS);

		
		try
		{
		    // Instantiate the FXMLLoader
			FXMLLoader loader = new FXMLLoader();
			
			// Set the location of the fxml file in the FXMLLoader
			loader.setLocation(MainAppFx.class.getResource("/fxml/MainForm.fxml"));
			
			// Localize the loader with its bundle
			// Uses the default locale and if a matching bundle is not found
			// will then use MessagesBundle.properties
			//loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
			
			// Parent is the base class for all nodes that have children in the
			// scene graph such as AnchorPane and most other containers
			Parent parent = (AnchorPane) loader.load();
			
			// Load the parent into a Scene
			Scene scene = new Scene(parent);
			
			// Put the Scene on Stage
			primaryStage.setScene(scene);
		} catch (IOException ex) {
		    log.error(null, ex);
		    //errorAlert(ex.getMessage());
		    System.exit(1);
		}
		
		// Set the window title
        //primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("TITLE"));
        // Raise the curtain on the Stage
        primaryStage.show();

	}
	
	/**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartText"));
        dialog.setContentText(msg);
        dialog.show();
    }


    public static void main(String[] args)
    {
    	Application.launch(args);
    }
}
