package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.WeekBean;

public class MonthlyCalendarController
{
	private int monthCounter = 0;
	
    @FXML private TableView<WeekBean> monthTableView;

    @FXML private TableColumn<WeekBean, String> sundayCol;

    @FXML private TableColumn<WeekBean, String> mondayCol;

    @FXML private TableColumn<WeekBean, String> tuesdayCol;

    @FXML private TableColumn<WeekBean, String> wednesdayCol;

    @FXML private TableColumn<WeekBean, String> thursdayCol;

    @FXML private TableColumn<WeekBean, String> fridayCol;

    @FXML private TableColumn<WeekBean, String> saturdayCol;
    
    @FXML private Label monthLabel;
    
    @FXML private void initialize()
    {
    	doBindings();
    }
    
    private void doBindings()
    {
    	sundayCol.setCellValueFactory(celldata -> celldata.getValue().getSundayProperty());
    	mondayCol.setCellValueFactory(celldata -> celldata.getValue().getMondayProperty());
    	tuesdayCol.setCellValueFactory(celldata -> celldata.getValue().getTuesdayProperty());
    	wednesdayCol.setCellValueFactory(celldata -> celldata.getValue().getWednesdayProperty());
    	thursdayCol.setCellValueFactory(celldata -> celldata.getValue().getThursdayProperty());
    	fridayCol.setCellValueFactory(celldata -> celldata.getValue().getFridayProperty());
    	saturdayCol.setCellValueFactory(celldata -> celldata.getValue().getSaturdayProperty());
    	setWidth();
    }
    
    private void setWidth() {
    	double width = monthTableView.getPrefWidth();
    	sundayCol.setPrefWidth(width * 0.4);
    	mondayCol.setPrefWidth(width * 0.4);
    	tuesdayCol.setPrefWidth(width * 0.4);
    	wednesdayCol.setPrefWidth(width * 0.4);
    	thursdayCol.setPrefWidth(width * 0.4);
    	fridayCol.setPrefWidth(width * 0.4);
    	saturdayCol.setPrefWidth(width * 0.4);   	
    }
    
    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTable() throws SQLException
    {
        // Add observable list data to the table
        monthTableView.setItems(findAllWeeks());
    }
    
    private ObservableList<WeekBean> findAllWeeks() throws SQLException
    {
    	ObservableList<WeekBean> weeks = FXCollections.observableArrayList();
    	
    	LocalDate currDate = LocalDate.now().plusMonths(monthCounter);
    	
    	monthLabel.setText(currDate.getMonth() + " " + currDate.getYear());

    	boolean beforeMonth = true;
    	
    	for(int i = 0; i < 6; i++)
    	{
    		WeekBean week = new WeekBean();
    		
    		int days = currDate.withDayOfMonth(1).getDayOfWeek().getValue();
    		
    		LocalDate baseDate = currDate.withDayOfMonth(1);
    		
    		if(days < 7)
    			baseDate = LocalDate.now().withDayOfMonth(1).minusDays(days);
    		
    		for(int j = 0; j < 7; j++)
    		{
    			AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    			
    			int day = baseDate.plusDays(j+(7*i)).getDayOfMonth();
    			
    			//if((i == 0 && day > 7) || ((i == 5 || i == 6) && day <= 7))
    				//continue;

        		List<AppointmentRecord> appointmentsDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), day).atStartOfDay()));
        		
        		String appointments = "";
        		
        		for(AppointmentRecord record : appointmentsDates)
            		appointments += record.getTitle() + "\n";
        			
        		switch(j)
        		{
        		case 0:
        			week.setSunday(""+day+"\n"+appointments);
        			break;
        		case 1:
        			week.setMonday(""+day+"\n"+appointments);
        			break;
        		case 2:
        			week.setTuesday(""+day+"\n"+appointments);
        			break;
        		case 3:
        			week.setWednesday(""+day+"\n"+appointments);
        			break;
        		case 4:
        			week.setThursday(""+day+"\n"+appointments);
        			break;
        		case 5:
        			week.setFriday(""+day+"\n"+appointments);
        			break;
        		case 6:
        			week.setSaturday(""+day+"\n"+appointments);
        			break;
        		}
    		}
    	
    		
        	
//    		int sunday = baseDate.plusDays(0+(7*i)).getDayOfMonth();
//    		int monday = baseDate.plusDays(1+(7*i)).getDayOfMonth();
//    		int tuesday = baseDate.plusDays(2+(7*i)).getDayOfMonth();
//    		int wednesday = baseDate.plusDays(3+(7*i)).getDayOfMonth();
//    		int thursday = baseDate.plusDays(4+(7*i)).getDayOfMonth();
//    		int friday = baseDate.plusDays(5+(7*i)).getDayOfMonth();
//    		int saturday = baseDate.plusDays(6+(7*i)).getDayOfMonth();
//    		
//    		String sundayAppointments = "";
//    		String mondayAppointments = "";
//    		String tuesdayAppointments = "";
//    		String wednesdayAppointments = "";
//    		String thursdayAppointments = "";
//    		String fridayAppointments = "";
//    		String saturdayAppointments = "";
//    		
//    		
//    		AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
//    		
//    		LocalDate sundayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), sunday);
//
//    		if((i == 0 && sunday < 7)  || (i == 6 && sunday < 7))
//    		{
//    			List<AppointmentRecord> sundayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), sunday).atStartOfDay()));
//    		
//    			for(AppointmentRecord record : sundayDates)
//        			sundayAppointments += record.getTitle() + "\n";
//    			
//    			week.setSunday(""+sunday+"\n"+sundayAppointments);
//    		}
//
//    		LocalDate mondayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), monday);
//
//    		if((i == 0 && monday < 7) || (i == 6))
//    		{
//    			List<AppointmentRecord> mondayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), monday).atStartOfDay()));
//    		    
//    	 		for(AppointmentRecord record : mondayDates)
//        			mondayAppointments += record.getTitle() + "\n";
//    	 		
//    	 		week.setMonday(""+monday+"\n"+mondayAppointments);
//    		}
//    			
//    		LocalDate tuesdayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), tuesday);
//
//    		if((i == 0 && tuesday < 7) || (i == 6))
//    		{
//    			List<AppointmentRecord> tuesdayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), tuesday).atStartOfDay()));
//    		    
//    			for(AppointmentRecord record : tuesdayDates)
//        			tuesdayAppointments += record.getTitle() + "\n";
//    			
//    			week.setTuesday(""+tuesday+"\n"+tuesdayAppointments);
//    		}
//    		
//    		LocalDate wednesdayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), wednesday);
//
//    		if((i == 0 && wednesday < 7) || (i == 6 && wednesday < 7))
//    		{
//    			List<AppointmentRecord> wednesdayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), wednesday).atStartOfDay()));
//    		    
//    	   		for(AppointmentRecord record : wednesdayDates)
//        			wednesdayAppointments += record.getTitle() + "\n";
//    	   		
//    	   		week.setWednesday(""+wednesday+"\n"+wednesdayAppointments);
//    		}
//    		
//    		LocalDate thursdayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), thursday);
//
//    		if((i == 0 && thursday < 7) || (i == 6 && thursday < 7))
//    		{
//    	  		List<AppointmentRecord> thursdayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), thursday).atStartOfDay()));
//    	  	  
//    	   		for(AppointmentRecord record : thursdayDates)
//        			thursdayAppointments += record.getTitle() + "\n";
//    	   		
//    	   		week.setThursday(""+thursday+"\n"+thursdayAppointments);
//    		}
    		
//    		LocalDate fridayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), friday);
//
//    		if((i == 0 && friday < 7) || (i == 6))
//    		{
//    			List<AppointmentRecord> fridayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), friday).atStartOfDay()));
//    	    	
//    	 		for(AppointmentRecord record : fridayDates)
//        			fridayAppointments += record.getTitle() + "\n";
//    	 		
//    	 		week.setFriday(""+friday+"\n"+fridayAppointments);
//    		}
//    		
//    		LocalDate saturdayDate = LocalDate.of(currDate.getYear(), currDate.getMonth(), saturday);
//    		
//    		if((i == 0 && saturday < 7) || (i == 6))
//    		{
//    			List<AppointmentRecord> saturdayDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), saturday).atStartOfDay()));
//    		
//   				for(AppointmentRecord record : saturdayDates)
//   					saturdayAppointments += record.getTitle() + "\n";
//   				
//   				week.setSaturday(""+saturday+"\n"+saturdayAppointments);
//    		}
//    		
    		weeks.add(week);
    		beforeMonth = false;
    	}
    	
    	return weeks;
    }
    
    private boolean isValidDate(int year, int month, int day)
    {
    	try
    	{
    		LocalDate.of(year, month, day);
    		return true;
    	}catch(DateTimeException e) {
    		return false;
    	}
    }

    @FXML private void onNextMonth(ActionEvent event) throws SQLException
    {
    	monthCounter++;
    	displayTable();
    }
    
    @FXML private void onPreviousMonth(ActionEvent event) throws SQLException
    {
    	monthCounter--;
    	displayTable();
    }
}
