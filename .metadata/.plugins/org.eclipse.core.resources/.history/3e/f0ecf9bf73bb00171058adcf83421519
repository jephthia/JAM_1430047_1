package jephthia.jam.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.DayBean;

public class DailyCalendarController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	
	private List<AppointmentRecord> currRecords;
	private LocalDate currDate = LocalDate.now();
	
    @FXML private Label dayLabel;
    @FXML private TableView<DayBean> dailyTableView;
    @FXML private TableColumn<DayBean, String> hoursCol;
    @FXML private TableColumn<DayBean, VBox> appointmentsCol;
    
    private ObservableList<TablePosition> cells;
    
    @FXML private void initialize() throws SQLException
    {
    	doBindings();
    	displayTable();
    }
    
    /**
     *  Bind our columns to our beans to display
     *  the appropriate appointments in the day
     */
    private void doBindings()
    {
    	hoursCol.setCellValueFactory(celldata -> celldata.getValue().getHourProperty());
    	appointmentsCol.setCellValueFactory(celldata -> celldata.getValue().getAppointmentsContainerProperty());
    	
    	// Set selection to a single cell
        dailyTableView.getSelectionModel().setCellSelectionEnabled(true);
        
        // Observable list of TablePosition objects that represent single cells
        cells = dailyTableView.getSelectionModel().getSelectedCells();

        // Listen for selection changes for a single cell amongst the selected cells
        cells.addListener(this::showEditAppointment);
    }
    
    private void showEditAppointment(ListChangeListener.Change<? extends TablePosition> change)
    {
    	if (cells.size() > 0) {
            TablePosition selectedCell = cells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();
            
            VBox container = (VBox)data;
            
            // The user clicked on a cell so show a form with the available
            // appointments so that they can choose which one they want to
            // update
    		try
    		{
    		    // Instantiate the FXMLLoader
    			FXMLLoader loader = new FXMLLoader();
    			
    			// Set the location of the fxml file in the FXMLLoader
    			loader.setLocation(MainController.class.getResource("/fxml/EditAppointmentsListForm.fxml"));
    			
    			// Localize the loader with its bundle
    			// Uses the default locale and if a matching bundle is not found
    			// will then use MessagesBundle.properties
    			loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
    			
    			// Parent is the base class for all nodes that have children in the
    			// scene graph such as AnchorPane and most other containers
    			Parent parent = (AnchorPane) loader.load();
    			
    			// Load the parent into a Scene
    			Scene scene = new Scene(parent);
    			
    			Stage stage = new Stage();
    			stage.setTitle("Appointment Form");
    			
    			// Put the Scene on Stage
    			stage.setScene(scene);
    			stage.show();
    			
    			EditAppointmentsListForm controller = loader.getController();
    			
    			AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    			
    			List<AppointmentRecord> appointments = new ArrayList<AppointmentRecord>();
    			
    			// Get all the appointments in the current selected cell
    			// and pass it to the controller so that we can display
    			// the daily view
    			for(Node node : container.getChildren())
    			{
    				Label titleLabel = (Label)node;
    				
    				List<AppointmentRecord> records = recordIO.getByTitle(titleLabel.getText());
    				
    				for(AppointmentRecord r : records)
    					appointments.add(r);
    			}
    			
    			controller.setAppointments(appointments);
    		} catch (IOException | SQLException ex) {
    		    log.error(null, ex);
    		}
        }
    }
    
    /**
     * Sets the current date of the daily calendar and
     * updates the table.
     * @param date
     */
    public void setCurrentDate(LocalDate date)
    {
    	this.currDate = date;
    	try {
			displayTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Displays the table with all the appointments
     * @throws SQLException
     */
    private void displayTable() throws SQLException
    {
    	dayLabel.setText(currDate.format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
    	
    	currRecords = getAppointments(currDate);
    	
    	dailyTableView.setItems(findHours());
    }
    
    /**
     * Used to get a list of all the appointments
     * for the given day
     * @param date
     * @return
     */
    private List<AppointmentRecord> getAppointments(LocalDate date)
    {
    	List<AppointmentRecord> records = new ArrayList<AppointmentRecord>();
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	try {
			records = recordIO.getByDate(Timestamp.valueOf(date.atStartOfDay()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return records;
    }
    
    /**
     * Sets the beans so that the table view can be
     * populated with the appropriate appointments.
     * @return
     * @throws SQLException
     */
    private ObservableList<DayBean> findHours() throws SQLException
    {
    	ObservableList<DayBean> hours = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < 24; i++)
    	{
    		VBox appointmentBox = new VBox();
    		VBox appointmentHalfBox = new VBox();
    		
    		LocalTime time = LocalTime.of(i, 0);
    		LocalTime halfTime = LocalTime.of(i, 30);
    		
    		/**
    		 * This takes care of finding the appointments that
    		 * match the current time, by looking at the start time
    		 * and the end time and seeing if it overlaps with the
    		 * current time.
    		 */
    		
    		for(AppointmentRecord record : currRecords)
    		{
    			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
    			AppointmentGroup group = groupIO.getRow(record.getAppointmentGroup());
    			
    			//Setting the font color of the appointments' title to the appointment's
    			//group color
    			Label appointmentLabel = new Label();
    			appointmentLabel.setTextFill(Paint.valueOf(group.getColor()));
    			Label appointmentHalfLabel = new Label();
    			appointmentHalfLabel.setTextFill(Paint.valueOf(group.getColor()));
    			
    			if(record.getEndTime().toLocalDateTime().toLocalDate().isEqual(currDate.atStartOfDay().toLocalDate()))
    			{
    				int endHour = record.getEndTime().toLocalDateTime().getHour();
	    			int endMinutes = record.getEndTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(endHour, endMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() >= 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isBefore(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isBefore(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			else if(currDate.isAfter(record.getStartTime().toLocalDateTime().toLocalDate()))
    			{
    				appointmentLabel.setText(record.getTitle());
    				appointmentHalfLabel.setText(record.getTitle());
    			}
    			else
    			{
	    			int startHour = record.getStartTime().toLocalDateTime().getHour();
	    			int startMinutes = record.getStartTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(startHour, startMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() < 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isAfter(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isAfter(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			
    			appointmentBox.getChildren().add(appointmentLabel);
    			appointmentHalfBox.getChildren().add(appointmentHalfLabel);
    		}
   
    		DayBean dayBean = new DayBean();
    		dayBean.setHour(i + ":" + "00");	
    		dayBean.setAppointmentsContainer(appointmentBox);
    		hours.add(dayBean);
    		
    		DayBean dayBeanHalf = new DayBean();
    		dayBeanHalf.setHour(i + ":" + "30");
    		dayBeanHalf.setAppointmentsContainer(appointmentHalfBox);
    		hours.add(dayBeanHalf);
    	}
    	
    	return hours;
    }
    
    @FXML private void onNextDay(ActionEvent event) throws SQLException
    {
    	setCurrentDate(currDate.plusDays(1));
    }

    @FXML private void onPreviousDay(ActionEvent event) throws SQLException
    {
    	setCurrentDate(currDate.minusDays(1));
    }
}
