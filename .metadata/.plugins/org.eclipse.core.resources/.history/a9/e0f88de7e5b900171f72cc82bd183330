package jephthia.jam.controllers;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.Properties;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentRecordsIO;

public class DbmsController
{
    @FXML private TextField urlTextField;

    @FXML private TextField dbnameTextField;

    @FXML private TextField usernameTextField;

    @FXML private TextField passwordTextField;

    @FXML private TextField portTextField;
    
    private Stage stage;
    private Scene mainScene;
    
    public void setStage(Stage stage, Scene mainScene)
    {
    	this.stage = stage;
    	this.mainScene = mainScene;
    }
    
    @FXML private void initialize() throws IOException
    {
    	Properties props = new Properties();
    	Path file = get("src/main/resources", "DBMS.properties");
    	
    	if(Files.exists(file))
    	{
    		try(InputStream is = newInputStream(file, StandardOpenOption.CREATE))
    		{
    			props.load(is);
    		}
    		
    		urlTextField.setText(props.getProperty("DatabaseUrl", ""));
    		dbnameTextField.setText(props.getProperty("DatabaseName", ""));
    		usernameTextField.setText(props.getProperty("DatabaseUsername", ""));
    		passwordTextField.setText(props.getProperty("DatabasePassword", ""));
    		portTextField.setText(props.getProperty("DatabasePort", ""));
    	}
    }

    @FXML private void handleSaveDbms(ActionEvent event) throws IOException
    {
    	Properties props = new Properties();
    	props.setProperty("DatabaseUrl", urlTextField.getText());
    	props.setProperty("DatabaseName", dbnameTextField.getText());
    	props.setProperty("DatabasePort", portTextField.getText());
    	props.setProperty("DatabaseUsername", usernameTextField.getText());
    	props.setProperty("DatabasePassword", passwordTextField.getText());
    	
    	Path file = get("src/main/resources", "DBMS.properties");
    	
    	try(OutputStream os = newOutputStream(file))
    	{
    		props.store(os, "DBMS settings");
    	}
    	
    	stage.setScene(mainScene);
    }
    
}