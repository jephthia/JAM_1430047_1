package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.WeekDayBean;

public class WeeklyCalendarController
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	private int weekCounter = 0;
	private List<AppointmentRecord> currRecords;
	private LocalDate currDate;
	
	@FXML private Label weekLabel;
	
    @FXML private TableView<WeekDayBean> weekTableView;

    @FXML private TableColumn<WeekDayBean, String> hourCol;
    
    @FXML private TableColumn<WeekDayBean, String> sundayCol;

    @FXML private TableColumn<WeekDayBean, String> mondayCol;

    @FXML private TableColumn<WeekDayBean, String> tuesdayCol;

    @FXML private TableColumn<WeekDayBean, String> wednesdayCol;

    @FXML private TableColumn<WeekDayBean, String> thursdayCol;

    @FXML private TableColumn<WeekDayBean, String> fridayCol;

    @FXML private TableColumn<WeekDayBean, String> saturdayCol;

    @FXML private void initialize() throws SQLException
    {
    	doBindings();
    	displayTable();
    }
    
    /**
     * Bind the columns to the bean so that
     * the table view can display the appropriate
     * appointments.
     */
    private void doBindings()
    {
    	hourCol.setCellValueFactory(celldata -> celldata.getValue().getHourProperty());
    	sundayCol.setCellValueFactory(celldata -> celldata.getValue().getSundayProperty());
    	mondayCol.setCellValueFactory(celldata -> celldata.getValue().getMondayProperty());
    	tuesdayCol.setCellValueFactory(celldata -> celldata.getValue().getTuesdayProperty());
    	wednesdayCol.setCellValueFactory(celldata -> celldata.getValue().getWednesdayProperty());
    	thursdayCol.setCellValueFactory(celldata -> celldata.getValue().getThursdayProperty());
    	fridayCol.setCellValueFactory(celldata -> celldata.getValue().getFridayProperty());
    	saturdayCol.setCellValueFactory(celldata -> celldata.getValue().getSaturdayProperty());
    }
    
    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTable() throws SQLException
    {
    	currDate = LocalDate.now().plusWeeks(weekCounter - 1).with(DayOfWeek.MONDAY);
        
        currRecords = getAppointments(currDate);
        
        // Add observable list data to the table
        weekTableView.setItems(findAllHours());
    }
    
    /**
     * Used to get a list of all the appointments
     * for the given day
     * @param date
     * @return
     */
    private List<AppointmentRecord> getAppointments(LocalDate date)
    {
    	List<AppointmentRecord> records = new ArrayList<AppointmentRecord>();
    	AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    	
    	try {
			records = recordIO.getByDate(Timestamp.valueOf(date.atStartOfDay()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    	return records;
    }
    
    /**
     * Finds all the appropriate appointments
     * that match the current time and adds it
     * to the bean so that the table view can
     * be populated
     * @return
     * @throws SQLException 
     */
    private ObservableList<WeekDayBean> findAllHours() throws SQLException
    {
    	weekLabel.setText("Week of " + LocalDate.now().plusWeeks(weekCounter - 1).with(DayOfWeek.SUNDAY).format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
    	
    	ObservableList<WeekDayBean> days = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < 24; i++)
    	{
    		VBox appointmentBox = new VBox();
    		VBox appointmentHalfBox = new VBox();
    		
    		LocalTime time = LocalTime.of(i, 0);
    		LocalTime halfTime = LocalTime.of(i, 30);
    		
    		/**
    		 * This takes care of finding the appointments that
    		 * match the current time, by looking at the start time
    		 * and the end time and seeing if it overlaps with the
    		 * current time.
    		 */
    		
    		for(AppointmentRecord record : currRecords)
    		{
    			AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
    			AppointmentGroup group = groupIO.getRow(record.getAppointmentGroup());
    			
    			//Setting the font color of the appointments' title to the appointment's
    			//group color
    			Label appointmentLabel = new Label();
    			appointmentLabel.setTextFill(Paint.valueOf(group.getColor()));
    			Label appointmentHalfLabel = new Label();
    			appointmentHalfLabel.setTextFill(Paint.valueOf(group.getColor()));
    			
    			if(record.getEndTime().toLocalDateTime().toLocalDate().isEqual(currDate.atStartOfDay().toLocalDate()))
    			{
    				int endHour = record.getEndTime().toLocalDateTime().getHour();
	    			int endMinutes = record.getEndTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(endHour, endMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() >= 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isBefore(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isBefore(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			else if(currDate.isAfter(record.getStartTime().toLocalDateTime().toLocalDate()))
    			{
    				appointmentLabel.setText(record.getTitle());
    				appointmentHalfLabel.setText(record.getTitle());
    			}
    			else
    			{
	    			int startHour = record.getStartTime().toLocalDateTime().getHour();
	    			int startMinutes = record.getStartTime().toLocalDateTime().getMinute();
	    			
	    			LocalTime currTime = LocalTime.of(startHour, startMinutes);
	    			
	    			if(time.getHour() == currTime.getHour())
	    			{
	    				if(currTime.getMinute() < 30)
	    					appointmentLabel.setText(record.getTitle());
	
	    				appointmentHalfLabel.setText(record.getTitle());
	    			}
	    			else
	    			{
	    				if(time.isAfter(currTime))
	    					appointmentLabel.setText(record.getTitle());
	    				if(halfTime.isAfter(currTime))
	    					appointmentHalfLabel.setText(record.getTitle());
	    			}
    			}
    			
    			appointmentBox.getChildren().add(appointmentLabel);
    			appointmentHalfBox.getChildren().add(appointmentHalfLabel);
    		}
    		
    		
    		WeekDayBean dayBean = new WeekDayBean();
    		dayBean.setHour(i + ":" + "00");
    		days.add(dayBean);
    		
    		WeekDayBean dayBeanHalf = new WeekDayBean();
    		dayBeanHalf.setHour(i + ":" + "30");
    		
    		days.add(dayBeanHalf);
    	}
    	
    	return days;
    }
    
    @FXML private void handleNextWeek(ActionEvent event) throws SQLException
    {
    	weekCounter++;
    	displayTable();
    }
    
    @FXML private void handlePreviousWeek(ActionEvent event) throws SQLException
    {
    	weekCounter--;
    	displayTable();
    }
}
