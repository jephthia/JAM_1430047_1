package jephthia.jam.controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.WeekBean;

public class MonthlyCalendarController
{
	private int monthCounter = 0;
	
    @FXML private TableView<WeekBean> monthTableView;

    @FXML private TableColumn<WeekBean, String> sundayCol;

    @FXML private TableColumn<WeekBean, String> mondayCol;

    @FXML private TableColumn<WeekBean, String> tuesdayCol;

    @FXML private TableColumn<WeekBean, String> wednesdayCol;

    @FXML private TableColumn<WeekBean, String> thursdayCol;

    @FXML private TableColumn<WeekBean, String> fridayCol;

    @FXML private TableColumn<WeekBean, String> saturdayCol;
    
    @FXML private Label monthLabel;
    
    private ObservableList<TablePosition> cells;
    
    @FXML private void initialize()
    {
    	doBindings();
    }
    
    private void doBindings()
    {
    	sundayCol.setCellValueFactory(celldata -> celldata.getValue().getSundayProperty());
    	mondayCol.setCellValueFactory(celldata -> celldata.getValue().getMondayProperty());
    	tuesdayCol.setCellValueFactory(celldata -> celldata.getValue().getTuesdayProperty());
    	wednesdayCol.setCellValueFactory(celldata -> celldata.getValue().getWednesdayProperty());
    	thursdayCol.setCellValueFactory(celldata -> celldata.getValue().getThursdayProperty());
    	fridayCol.setCellValueFactory(celldata -> celldata.getValue().getFridayProperty());
    	saturdayCol.setCellValueFactory(celldata -> celldata.getValue().getSaturdayProperty());
    	setWidth();
    	
        // Set selection to a single cell
        monthTableView.getSelectionModel().setCellSelectionEnabled(true);

        // Listen for selection changes and show the selected fishData details
        // when a new row is selected.
//        monthTableView
//                .getSelectionModel()
        
//                .selectedItemProperty()
//                .addListener(
//                        (observable, oldValue, newValue) -> showAppointmentDetails(newValue));

        // Observable list of TablePosition objects that represent single cells
        cells = monthTableView.getSelectionModel().getSelectedCells();

        // Listen for selection changes for a single cell amongst the selected cells
        cells.addListener(this::showDailyView);
    }
    
    private void showDailyView(ListChangeListener.Change<? extends TablePosition> change)
    {
    	if (cells.size() > 0) {
            TablePosition selectedCell = cells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();
            
            LocalDate dateSelected = LocalDate.of(LocalDate.now().plusMonths(monthCounter).getYear(),
            							LocalDate.now().plusMonths(monthCounter).getMonth(),
            							Integer.parseInt(data.toString().substring(0, 3).trim()));
          
            
            
            //log.info("value = " + (String) data);
        }
    }
    
    private void setWidth() {
    	double width = monthTableView.getPrefWidth();
    	sundayCol.setPrefWidth(width * 0.4);
    	mondayCol.setPrefWidth(width * 0.4);
    	tuesdayCol.setPrefWidth(width * 0.4);
    	wednesdayCol.setPrefWidth(width * 0.4);
    	thursdayCol.setPrefWidth(width * 0.4);
    	fridayCol.setPrefWidth(width * 0.4);
    	saturdayCol.setPrefWidth(width * 0.4);   	
    }
    
    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTable() throws SQLException
    {
        // Add observable list data to the table
        monthTableView.setItems(findAllWeeks());
    }
    
    private ObservableList<WeekBean> findAllWeeks() throws SQLException
    {
    	ObservableList<WeekBean> weeks = FXCollections.observableArrayList();
    	
    	LocalDate currDate = LocalDate.now().plusMonths(monthCounter);
    	
    	monthLabel.setText(currDate.getMonth() + " " + currDate.getYear());
    	
    	for(int i = 0; i < 6; i++)
    	{
    		WeekBean week = new WeekBean();
    		
    		int days = currDate.withDayOfMonth(1).getDayOfWeek().getValue();
    		
    		LocalDate baseDate = currDate.withDayOfMonth(1);
    		
    		if(days < 7)
    			baseDate = LocalDate.now().withDayOfMonth(1).minusDays(days);
    		
    		for(int j = 0; j < 7; j++)
    		{
    			AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    			
    			int day = baseDate.plusDays(j+(7*i)).getDayOfMonth();
    			
    			if((i == 0 && day > 7) || ((i == 4 || i == 5) && day <= 13))
    				continue;
    			
    			if(!isValidDate(currDate.getYear(), currDate.getMonth().getValue(), day))
    				continue;

        		List<AppointmentRecord> appointmentsDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), day).atStartOfDay()));
        		
        		String appointments = "";
        		
        		for(AppointmentRecord record : appointmentsDates)
            		appointments += record.getTitle() + "\n";
        			
        		switch(j)
        		{
        		case 0:
        			week.setSunday(""+day+"\n"+appointments);
        			break;
        		case 1:
        			week.setMonday(""+day+"\n"+appointments);
        			break;
        		case 2:
        			week.setTuesday(""+day+"\n"+appointments);
        			break;
        		case 3:
        			week.setWednesday(""+day+"\n"+appointments);
        			break;
        		case 4:
        			week.setThursday(""+day+"\n"+appointments);
        			break;
        		case 5:
        			week.setFriday(""+day+"\n"+appointments);
        			break;
        		case 6:
        			week.setSaturday(""+day+"\n"+appointments);
        			break;
        		}
    		}
    		
    		weeks.add(week);
    	}
    	
    	return weeks;
    }
    
    private boolean isValidDate(int year, int month, int day)
    {
    	try
    	{
    		LocalDate.of(year, month, day);
    		return true;
    	}catch(DateTimeException e) {
    		return false;
    	}
    }

    @FXML private void onNextMonth(ActionEvent event) throws SQLException
    {
    	monthCounter++;
    	displayTable();
    }
    
    @FXML private void onPreviousMonth(ActionEvent event) throws SQLException
    {
    	monthCounter--;
    	displayTable();
    }
}
