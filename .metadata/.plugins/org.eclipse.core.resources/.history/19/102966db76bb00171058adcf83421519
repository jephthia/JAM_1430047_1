package jephthia.jam.crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jephthia.jam.ConnectionJDBC;
import jephthia.jam.entities.AppointmentRecord;

/**
 * @author Jephthia Louis
 */
public class AppointmentRecordsIO
{
    private Logger log = LoggerFactory.getLogger(this.getClass().getName());
     
    /**
     * Adds a new Appointment Record and updates
     * the record passed with the new id.
     * @param record
     * @return
     * @throws SQLException
     */
    public int addRow(AppointmentRecord record) throws SQLException
    {
    	int insertedID = -1;
    	
    	String query = "{call appointment_records_add(?,?,?,?,?,?,?,?)}";
    	
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{	
			callSmtp.setString(1, record.getTitle());
			callSmtp.setString(2, record.getLocation());
			callSmtp.setTimestamp(3, record.getStartTime());
			callSmtp.setTimestamp(4, record.getEndTime());
			callSmtp.setString(5, record.getDetails());
			callSmtp.setBoolean(6, record.isWholeDay());
			callSmtp.setInt(7, record.getAppointmentGroup());
			callSmtp.setBoolean(8, record.isAlarmReminder());

			callSmtp.executeUpdate();
			
			log.debug("appointment record added");
			
			String lastID = "select last_insert_id()";
			
			PreparedStatement stmt = con.prepareStatement(lastID);
			ResultSet resultSet = stmt.executeQuery();
			
			if(resultSet.next())
				insertedID = resultSet.getInt("last_insert_id()");
			
			record.setID(insertedID);
			
		} catch (SQLException e) {
			throw e;
		}
		
		return insertedID;
    }
    
    /**
     * Updates the appointment record, if no
     * appointment record is found with the same
     * id, it will do nothing
     * @param record
     * @throws SQLException
     */
    public void updateRow(AppointmentRecord record) throws SQLException
    {
    	String query = "{call appointment_records_update(?,?,?,?,?,?,?,?,?)}";
    	
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{	
			callSmtp.setInt(1, record.getID());
			callSmtp.setString(2, record.getTitle());
			callSmtp.setString(3, record.getLocation());
			callSmtp.setTimestamp(4, record.getStartTime());
			callSmtp.setTimestamp(5, record.getEndTime());
			callSmtp.setString(6, record.getDetails());
			callSmtp.setBoolean(7, record.isWholeDay());
			callSmtp.setInt(8, record.getAppointmentGroup());
			callSmtp.setBoolean(9, record.isAlarmReminder());

			callSmtp.executeUpdate();
			
			log.debug("appointment record updated");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    /**
     * Deletes the appointment records that
     * matches the id, if no record matches
     * the id, it will do nothing.
     * @param id
     * @throws SQLException
     */
    public void deleteRow(int id) throws SQLException
    {
    	String query = "{call appointment_records_delete(?)}";
    	
		try(Connection con = new ConnectionJDBC().getConnection();
			CallableStatement callSmtp = con.prepareCall(query))
		{	
			callSmtp.setInt(1, id);
			callSmtp.executeUpdate();
			
			log.debug("appointment record deleted");
		} catch (SQLException e) {
			throw e;
		}
    }
    
    /**
     * Returns a list of appointment records
     * that match the current date, if no
     * appointments are found it will return
     * an empty list. 
     * @param date
     * @return
     * @throws SQLException
     */
    public List<AppointmentRecord> getByDate(Timestamp date) throws SQLException
    {
    	log.debug("appointment records getByDate called");
        
    	String query = "select * from appointment_records where date(?) BETWEEN date(start_time) AND date(end_time)";
    	
		List<AppointmentRecord> records;
		
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{		
			stmt.setTimestamp(1, date);
			
			ResultSet results = stmt.executeQuery();
    
			records = new ArrayList<AppointmentRecord>();
			
			while(results.next())
			{	
				AppointmentRecord record = new AppointmentRecord();
				record.setID(results.getInt("id"));
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    records.add(record);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return records;
    }
    
    public List<AppointmentRecord> getByTitle(String title) throws SQLException
    {
    	log.debug("appointment records getByTitle called");
        
    	String query = "select * from appointment_records where title = ?";
    	
		List<AppointmentRecord> records;
		
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{
			stmt.setString(1, title);
			
			ResultSet results = stmt.executeQuery();
			
			records = new ArrayList<AppointmentRecord>();
    
			while(results.next())
			{	
				AppointmentRecord record = new AppointmentRecord();
				record.setID(results.getInt("id"));
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    records.add(record);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return records;
    }
    
    public List<AppointmentRecord> getBetweenDates(Timestamp start, Timestamp end) throws SQLException
    {
    	log.debug("appointment records getBetweenDates called");
        
    	String query = "select * from appointment_records where start_time between ? and ?";
    	
		List<AppointmentRecord> records;
		
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{			
			stmt.setTimestamp(1, start);
			stmt.setTimestamp(2, end);
			
			ResultSet results = stmt.executeQuery();
    
			records = new ArrayList<AppointmentRecord>();
			
			while(results.next())
			{	
				AppointmentRecord record = new AppointmentRecord();
				record.setID(results.getInt("id"));
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    records.add(record);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return records;
    }
    
    public List<AppointmentRecord> getByMonth(Timestamp month) throws SQLException
    {
    	log.debug("appointment records getByMonth called");
        
    	String query = "select * from appointment_records where month(start_time) = month(?)";
    	
		List<AppointmentRecord> records;
		
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{		
			stmt.setTimestamp(1, month);
			
			ResultSet results = stmt.executeQuery();
    
			records = new ArrayList<AppointmentRecord>();
			
			while(results.next())
			{	
				AppointmentRecord record = new AppointmentRecord();
				record.setID(results.getInt("id"));
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    records.add(record);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return records;
    }
    
    public List<AppointmentRecord> getByWeek(Timestamp week) throws SQLException
    {
    	log.debug("appointment records getByWeek called");
        
    	String query = "select * from appointment_records where week(start_time) = week(?)";
    	
		List<AppointmentRecord> records;
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{
			stmt.setTimestamp(1, week);
			
			ResultSet results = stmt.executeQuery();
			
			records = new ArrayList<AppointmentRecord>();
			
			while(results.next())
			{	
				AppointmentRecord record = new AppointmentRecord();
				record.setID(results.getInt("id"));
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    records.add(record);
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return records;
    }
    
    public AppointmentRecord getByID(int id) throws SQLException
    {
        log.debug("appointment records getByID called");
        
        String query = "select * from appointment_records where id = ?";
        
		try(Connection con = new ConnectionJDBC().getConnection();
			PreparedStatement stmt = con.prepareStatement(query))
		{
			stmt.setInt(1, id);
			
			ResultSet results = stmt.executeQuery();
    
			if(results.next())
			{
			    AppointmentRecord record = new AppointmentRecord();
			    record.setID(id);
			    record.setTitle(results.getString("title"));
			    record.setLocation(results.getString("location"));
			    record.setStartTime(results.getTimestamp("start_time"));
			    record.setEndTime(results.getTimestamp("end_time"));
			    record.setDetails(results.getString("details"));
			    record.setWholeDay(results.getBoolean("whole_day"));
			    record.setAppointmentGroup(results.getInt("appointment_group"));
			    record.setAlarmReminder(results.getBoolean("alarm_reminder"));
			    
			    return record;
			}
		} catch (SQLException e) {
			throw e;
		}
        
        return null;
    }
}
