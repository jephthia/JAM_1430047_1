package jephthia.jam.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import jephthia.jam.crud.AppointmentGroupsIO;
import jephthia.jam.crud.AppointmentRecordsIO;
import jephthia.jam.entities.AppointmentGroup;
import jephthia.jam.entities.AppointmentRecord;
import jephthia.jam.entities.WeekBean;

public class MonthlyCalendarController
{
	private int monthCounter = 0;
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	
    @FXML private TableView<WeekBean> monthTableView;

    @FXML private TableColumn<WeekBean, VBox> sundayCol;

    @FXML private TableColumn<WeekBean, VBox> mondayCol;

    @FXML private TableColumn<WeekBean, VBox> tuesdayCol;

    @FXML private TableColumn<WeekBean, VBox> wednesdayCol;

    @FXML private TableColumn<WeekBean, VBox> thursdayCol;

    @FXML private TableColumn<WeekBean, VBox> fridayCol;

    @FXML private TableColumn<WeekBean, VBox> saturdayCol;
    
    @FXML private Label monthLabel;
    
    private ObservableList<TablePosition> cells;
    
    @FXML private void initialize()
    {
    	doBindings();
    }
    
    private void doBindings()
    {
    	sundayCol.setCellValueFactory(celldata -> celldata.getValue().getSundayProperty());
    	mondayCol.setCellValueFactory(celldata -> celldata.getValue().getMondayProperty());
    	tuesdayCol.setCellValueFactory(celldata -> celldata.getValue().getTuesdayProperty());
    	wednesdayCol.setCellValueFactory(celldata -> celldata.getValue().getWednesdayProperty());
    	thursdayCol.setCellValueFactory(celldata -> celldata.getValue().getThursdayProperty());
    	fridayCol.setCellValueFactory(celldata -> celldata.getValue().getFridayProperty());
    	saturdayCol.setCellValueFactory(celldata -> celldata.getValue().getSaturdayProperty());
    	setWidth();
    	
        // Set selection to a single cell
        monthTableView.getSelectionModel().setCellSelectionEnabled(true);

        // Listen for selection changes and show the selected fishData details
        // when a new row is selected.
//        monthTableView
//                .getSelectionModel()
        
//                .selectedItemProperty()
//                .addListener(
//                        (observable, oldValue, newValue) -> showAppointmentDetails(newValue));

        // Observable list of TablePosition objects that represent single cells
        cells = monthTableView.getSelectionModel().getSelectedCells();

        // Listen for selection changes for a single cell amongst the selected cells
        cells.addListener(this::showDailyView);
    }
    
    private void showDailyView(ListChangeListener.Change<? extends TablePosition> change)
    {
    	if (cells.size() > 0) {
            TablePosition selectedCell = cells.get(0);
            TableColumn column = selectedCell.getTableColumn();
            int rowIndex = selectedCell.getRow();
            Object data = column.getCellObservableValue(rowIndex).getValue();
            
            LocalDate dateSelected = LocalDate.of(LocalDate.now().plusMonths(monthCounter).getYear(),
            							LocalDate.now().plusMonths(monthCounter).getMonth(),
            							Integer.parseInt(data.toString().split("\n")[0]));
          
    		log.info("Handle Open Daily Calendar");
    		
    		try
    		{
    		    // Instantiate the FXMLLoader
    			FXMLLoader loader = new FXMLLoader();
    			
    			// Set the location of the fxml file in the FXMLLoader
    			loader.setLocation(MainController.class.getResource("/fxml/DailyCalendar.fxml"));
    			
    			// Localize the loader with its bundle
    			// Uses the default locale and if a matching bundle is not found
    			// will then use MessagesBundle.properties
    			//loader.setResources(ResourceBundle.getBundle("MessagesBundle"));
    			
    			// Parent is the base class for all nodes that have children in the
    			// scene graph such as AnchorPane and most other containers
    			Parent parent = (BorderPane) loader.load();
    			
    			// Load the parent into a Scene
    			Scene scene = new Scene(parent);
    			
    			Stage stage = new Stage();
    			stage.setTitle("Daily Calendar");
    			
    			// Put the Scene on Stage
    			stage.setScene(scene);
    			stage.show();
    			
    			DailyCalendarController controller = loader.getController();
    			controller.setCurrentDate(dateSelected);
    			System.out.println(dateSelected.toString());
    			//((Node)(event.getSource())).getScene().getWindow().hide();
    		} catch (IOException ex) {
    		    log.error(null, ex);
    		}
            
            //log.info("value = " + (String) data);
        }
    }
    
    private void setWidth() {
    	double width = monthTableView.getPrefWidth();
    	sundayCol.setPrefWidth(width * 0.4);
    	mondayCol.setPrefWidth(width * 0.4);
    	tuesdayCol.setPrefWidth(width * 0.4);
    	wednesdayCol.setPrefWidth(width * 0.4);
    	thursdayCol.setPrefWidth(width * 0.4);
    	fridayCol.setPrefWidth(width * 0.4);
    	saturdayCol.setPrefWidth(width * 0.4);   	
    }
    
    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTable() throws SQLException
    {
        // Add observable list data to the table
        monthTableView.setItems(findAllWeeks());
    }
    
    private ObservableList<WeekBean> findAllWeeks() throws SQLException
    {
    	ObservableList<WeekBean> weeks = FXCollections.observableArrayList();
    	
    	AppointmentGroupsIO groupIO = new AppointmentGroupsIO();
    	
    	LocalDate currDate = LocalDate.now().plusMonths(monthCounter);
    	
    	monthLabel.setText(currDate.getMonth() + " " + currDate.getYear());
    	
    	for(int i = 0; i < 6; i++)
    	{
    		WeekBean week = new WeekBean();
    		
    		int days = currDate.withDayOfMonth(1).getDayOfWeek().getValue();
    		
    		LocalDate baseDate = currDate.withDayOfMonth(1);
    		
    		if(days < 7)
    			baseDate = LocalDate.now().withDayOfMonth(1).minusDays(days);
    		
    		for(int j = 0; j < 7; j++)
    		{	
    			VBox dayContainer = new VBox();
    			
    			AppointmentRecordsIO recordIO = new AppointmentRecordsIO();
    			
    			int day = baseDate.plusDays(j+(7*i)).getDayOfMonth();
    			
    			if((i == 0 && day > 7) || ((i == 4 || i == 5) && day <= 13))
    				continue;
    			
    			if(!isValidDate(currDate.getYear(), currDate.getMonth().getValue(), day))
    				continue;

    			dayContainer.getChildren().add(new Label(""+day));
    			
        		List<AppointmentRecord> appointmentsDates = recordIO.getByDate(Timestamp.valueOf(LocalDate.of(currDate.getYear(), currDate.getMonth(), day).atStartOfDay()));
        		
        		for(AppointmentRecord record : appointmentsDates)
        		{		
        			AppointmentGroup group = groupIO.getRow(record.getAppointmentGroup());
        			
        			Label appointmentLabel = new Label();
        			appointmentLabel.setText(record.getTitle());
        			log.debug("COLOR: " + group.getColor());
        			appointmentLabel.setTextFill(Paint.valueOf(group.getColor()));
            		dayContainer.getChildren().add(appointmentLabel);
        		}
        		
        		switch(j)
        		{
	        		case 0:
	        			week.setSunday(dayContainer);
	        			break;
	        		case 1:
	        			week.setMonday(dayContainer);
	        			break;
	        		case 2:
	        			week.setTuesday(dayContainer);
	        			break;
	        		case 3:
	        			week.setWednesday(dayContainer);
	        			break;
	        		case 4:
	        			week.setThursday(dayContainer);
	        			break;
	        		case 5:
	        			week.setFriday(dayContainer);
	        			break;
	        		case 6:
	        			week.setSaturday(dayContainer);
	        			break;
        		}
    		}
    		
    		weeks.add(week);
    	}
    	
    	return weeks;
    }
    
    private boolean isValidDate(int year, int month, int day)
    {
    	try
    	{
    		LocalDate.of(year, month, day);
    		return true;
    	}catch(DateTimeException e) {
    		return false;
    	}
    }

    @FXML private void onNextMonth(ActionEvent event) throws SQLException
    {
    	monthCounter++;
    	displayTable();
    }
    
    @FXML private void onPreviousMonth(ActionEvent event) throws SQLException
    {
    	monthCounter--;
    	displayTable();
    }
}
